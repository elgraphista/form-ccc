import {Component} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    gdd: any;
    ionite: any;
    puntoOrigen: any;
    puntoRetiro: any;
    puntoDestino: any;
    form: FormGroup;
    date: any;

    constructor() {
        this.form = new FormGroup({
            date: new FormControl(),
            ionite: new FormControl(),
            puntoOrigen: new FormControl(),
            puntoRetiro: new FormControl(),
            puntoDestino: new FormControl()
        });
    }

    inputDocumentReceptionModal() {
        console.log('Ok');
    }

    getLpnForGdd() {
        console.log('Ok');
    }
}

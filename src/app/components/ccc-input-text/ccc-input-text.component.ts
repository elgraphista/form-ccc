import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'ccc-input-text',
    templateUrl: './ccc-input-text.component.html',
    styleUrls: ['./ccc-input-text.component.scss'],
})
export class CccInputTextComponent implements OnInit {
    modelValue: string;

    @Output()
    modelChange = new EventEmitter<string>();

    @Input()
    get model() {
        return this.modelValue;
    }

    set model(val) {
        this.modelValue = val;
        this.modelChange.emit(this.modelValue);
    }

    @Input() placeholder: string;
    @Input() type: string;
    @Input() icon: string;
    @Input() hasModal: boolean;
    public validatedIcon: string;
    public validatedModal: boolean;
    public inputFocused: boolean;

    @Output() modalCallback: EventEmitter<any> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
        this.validatedIcon = typeof this.icon === 'string' &&
        this.icon.length > 0 ? this.icon : '';
        this.validatedModal = typeof this.hasModal === 'boolean' ? this.hasModal : false;
    }

    evaluateTitle() {
        this.inputFocused = !this.inputFocused;
    }

    suggestionWasClicked(): void {
        this.modalCallback.emit();
    }

}
